# README #

This project is based on Client/Server example from "http://www.mpi-forum.org".

Link to the example:
[client/server example](http://www.mpi-forum.org/docs/mpi-2.0/mpi-20-html/node106.htm#Node106)


If you want to use it you should firstly download and install MSMPI software from:

[link to the msdn](https://msdn.microsoft.com/en-us/library/bb524831(v=vs.85).aspx)

[link to the latest MSMPI](https://www.microsoft.com/en-us/download/details.aspx?id=47259)