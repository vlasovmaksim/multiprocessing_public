#include <mpi.h>
#include <stdio.h>
#include <fstream>

#define MAX_DATA MPI_MAX_DATAREP_STRING


int main(int argc, char** argv) {
  MPI_Comm client;
  MPI_Status status;
  char port_name[MPI_MAX_PORT_NAME];

  double buf[MAX_DATA];
  int size, again;

  MPI_Init(&argc, &argv);
  MPI_Comm_size(MPI_COMM_WORLD, &size);


  double a1 = 1.0;
  //double b = dolfin::MPI::sum<double>(MPI_COMM_WORLD, a1);

  double out;
  //MPI_Allreduce(const_cast<double*>(&a), &out, 1, MPI_DOUBLE, MPI_MAX, MPI_COMM_WORLD);
  //MPI_Allreduce(&a1, &out, 1, MPI_DOUBLE, MPI_MAX, MPI_COMM_WORLD);
  MPI_Allreduce(&a1, &out, 1, MPI_DOUBLE, MPI_MAX, MPI_COMM_WORLD);


  if (size != 1) {
    //error(FATAL, "Server too big");
    printf("Server too big");
  }

  MPI_Open_port(MPI_INFO_NULL, port_name);
  printf("server available at %s\n", port_name);


#pragma region No need. MPI_Publish_name().
  //int error_code = 0;

  //MPI_Info info;
  //MPI_Info_create(&info);
  //MPI_Info_set(info, "ompi_global_scope", "true");
  //error_code = MPI_Publish_name("ocean", info, port_name);  

  //error_code = MPI_Publish_name("ocean", MPI_INFO_NULL, port_name);
  //printf("error code = %d \n", error_code);
#pragma endregion


#pragma region Write a port to a file.
  // Write a port to a file.
  // A client then could read the file and establish connection.
  std::ofstream file_port;
  file_port.open("port.dat", std::ios::out);
  file_port << port_name << std::endl;
  file_port.close();
#pragma endregion

  
  while (1) {
    MPI_Comm_accept(port_name, MPI_INFO_NULL, 0, MPI_COMM_WORLD,
                    &client);
    again = 1;
    while (again) {
      MPI_Recv(buf, MAX_DATA, MPI_DOUBLE,
               MPI_ANY_SOURCE, MPI_ANY_TAG, client, &status);

      switch (status.MPI_TAG) {
        case 0:
          MPI_Comm_free(&client);


#pragma region No need. MPI_Unpublish_name().
          //MPI_Unpublish_name("ocean", MPI_INFO_NULL, port_name);  
#pragma endregion


          MPI_Close_port(port_name);
          MPI_Finalize();
          return 0;
        case 1:
          MPI_Comm_disconnect(&client);
          again = 0;
          break;
        case 2: /* do something */
          printf("A client connected to the server \n");
          printf("rank = %f; size = %f \n", buf[0], buf[1]);
          break;
        default:
          /* Unexpected message type */
          MPI_Abort(MPI_COMM_WORLD, 1);
      }
    }
  }
}
