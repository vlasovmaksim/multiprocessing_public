#include <mpi.h>
#include <stdio.h>
#include <string.h>
#include <fstream>
#include <string>

#define MAX_DATA MPI_MAX_DATAREP_STRING


int main(int argc, char** argv) {

  int rank, size, len;
  char name[MPI_MAX_PROCESSOR_NAME];

  MPI_Comm server;
  double buf[MAX_DATA];
  char port_name[MPI_MAX_PORT_NAME];


#pragma region Read a port from a file.
  // A client read a port from a file to establish connection to a server.
  std::ifstream file_port;
  file_port.open("port.dat", std::ios::in);

  std::string port_name_input;
  std::string line;

  while (std::getline(file_port, line)) {
    port_name_input = line;
  }

  file_port.close();

  //printf("port_name_input = %s\n", port_name_input.c_str());

  size_t pos = port_name_input.find(':');
  std::string temp = port_name_input.substr(pos + 1);

  strcpy(port_name, temp.c_str());
#pragma endregion

  
  MPI_Init(&argc, &argv);
  //strcpy(port_name, argv[1]);/* assume server's name is cmd-line arg */


#pragma region No need. MPI_Lookup_name().
  //int error_code = 0;
  //error_code = MPI_Lookup_name("ocean", MPI_INFO_NULL, port_name);
  //printf("error code = %d \n", error_code);  
#pragma endregion


  printf("recieve port name = %s\n", port_name);

  MPI_Comm_connect(port_name, MPI_INFO_NULL, 0, MPI_COMM_WORLD,
                   &server);

  bool done = false;
  int tag;

  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  MPI_Comm_size(MPI_COMM_WORLD, &size);
  MPI_Get_processor_name(name, &len);

  printf("rank = %d; size = %d; name = %s \n", rank, size, name);

  //while (!done) {
  //  tag = 2; /* Action to perform */

  //  MPI_Send(buf, MAX_DATA, MPI_DOUBLE, 0, tag, server);
  //  /* etc */
  //}

  tag = 2;
  buf[0] = rank;
  buf[1] = size;
  MPI_Send(buf, MAX_DATA, MPI_DOUBLE, 0, tag, server);

  MPI_Send(buf, 0, MPI_DOUBLE, 0, 1, server);
  MPI_Comm_disconnect(&server);
  MPI_Finalize();
  return 0;
}
