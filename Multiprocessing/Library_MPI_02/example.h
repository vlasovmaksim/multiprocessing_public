#pragma once


#ifdef __cplusplus    // If used by C++ code, 
extern "C" {          // we need to export the C interface
#endif

  __declspec(dllexport) void __stdcall run();

  __declspec(dllexport) void __stdcall print_something();

#ifdef __cplusplus
}
#endif