#include <mpi.h>
#include <fstream>
#include <stdio.h>
#include <string>
#include <windows.h> 
#include <memory.h>

#include "example.h"

#define MAX_DATA MPI_MAX_DATAREP_STRING


int __stdcall RunMPIServer() {
  MPI_Comm client;
  MPI_Status status;
  char port_name[MPI_MAX_PORT_NAME];

  double buf[MAX_DATA];
  int size, again;

  //MPI_Init(&argc, &argv);
  MPI_Init(NULL, NULL);
  MPI_Comm_size(MPI_COMM_WORLD, &size);

  if (size != 1) {
    //error(FATAL, "Server too big");
    printf("Server too big");
  }

  MPI_Open_port(MPI_INFO_NULL, port_name);
  printf("server available at %s\n", port_name);


#pragma region Write a port to a file.
  // Write a port to a file.
  // A client then could read the file and establish connection.
  std::ofstream file_port;
  file_port.open("port.dat", std::ios::out);
  file_port << port_name << std::endl;
  file_port.close();
#pragma endregion


  while (1) {
    MPI_Comm_accept(port_name, MPI_INFO_NULL, 0, MPI_COMM_WORLD,
      &client);
    again = 1;
    while (again) {
      MPI_Recv(buf, MAX_DATA, MPI_DOUBLE,
        MPI_ANY_SOURCE, MPI_ANY_TAG, client, &status);

      switch (status.MPI_TAG) {
      case 0:
        MPI_Comm_free(&client);

        MPI_Close_port(port_name);
        MPI_Finalize();
        return 0;
      case 1:
        MPI_Comm_disconnect(&client);
        again = 0;
        break;
      case 2: /* do something */
        printf("A client connected to the server \n");
        printf("rank = %f; size = %f \n", buf[0], buf[1]);
        break;
      default:
        /* Unexpected message type */
        MPI_Abort(MPI_COMM_WORLD, 1);
      }
    }
  }
}

int __stdcall RunClient() {
  int rank, size, len;
  char name[MPI_MAX_PROCESSOR_NAME];

  MPI_Comm server;
  double buf[MAX_DATA];
  char port_name[MPI_MAX_PORT_NAME];


#pragma region Read a port from a file.
  // A client read a port from a file to establish connection to a server.
  std::ifstream file_port;
  file_port.open("port.dat", std::ios::in);

  std::string port_name_input;
  std::string line;

  while (std::getline(file_port, line)) {
    port_name_input = line;
  }

  file_port.close();

  //printf("port_name_input = %s\n", port_name_input.c_str());

  size_t pos = port_name_input.find(':');
  std::string temp = port_name_input.substr(pos + 1);

  strcpy(port_name, temp.c_str());
#pragma endregion
  

  MPI_Init(NULL, NULL);
  //strcpy(port_name, argv[1]);/* assume server's name is cmd-line arg */

  printf("recieve port name = %s\n", port_name);

  MPI_Comm_connect(port_name, MPI_INFO_NULL, 0, MPI_COMM_WORLD,
    &server);

  bool done = false;
  int tag;

  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  MPI_Comm_size(MPI_COMM_WORLD, &size);
  MPI_Get_processor_name(name, &len);

  printf("rank = %d; size = %d; name = %s \n", rank, size, name);

  tag = 2;
  buf[0] = rank;
  buf[1] = size;
  MPI_Send(buf, MAX_DATA, MPI_DOUBLE, 0, tag, server);

  MPI_Send(buf, 0, MPI_DOUBLE, 0, 1, server);
  MPI_Comm_disconnect(&server);
  MPI_Finalize();
  return 0;
}

void __stdcall PrintSomething() {
  printf("Hi from library! \n");
}


// Link to the documentation:
// https://msdn.microsoft.com/en-us/library/windows/desktop/ms682596(v=vs.85).aspx
// https://msdn.microsoft.com/en-us/library/windows/desktop/ms682583(v=vs.85).aspx

/// Entry point in the dll.
BOOL APIENTRY DllMain(HANDLE dll_handle, // DLL module handle
                      DWORD reason, // reason called 
                      LPVOID reserved) // reserved 
{
  switch (reason) {
      // DLL load due to process initialization or LoadLibrary

    case DLL_PROCESS_ATTACH:
      printf("A process is loading the DLL. \n");

      RunMPIServer();

      break;

    case DLL_PROCESS_DETACH:
      printf("A process is unloading the DLL. \n");
      break;

    default:
      break;
  }

  return TRUE;
}

