#pragma once


#ifdef __cplusplus    // If used by C++ code, 
extern "C" {          // we need to export the C interface
#endif

  // Run MPI server.
  __declspec(dllexport) int __stdcall RunMPIServer();

  // Run client.
  __declspec(dllexport) int __stdcall RunClient();

  // Sample function.
  __declspec(dllexport) void __stdcall PrintSomething();

#ifdef __cplusplus
}
#endif